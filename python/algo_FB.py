## Here we implement the TR-VMFB algorithm
import numpy as np
from scipy.optimize import minimize
from sklearn.datasets import fetch_california_housing, make_regression
from sklearn.model_selection import train_test_split
from sklearn.linear_model import Lasso, Ridge
from sklearn.metrics import mean_squared_error
from tqdm import tqdm

from utils import *


def sparcity(x, epslion=0.001):
    """ Nombre de coefficients nuls. """
    return np.sum(abs(x) < epslion)


california_housing = fetch_california_housing(as_frame=True)
X = california_housing.data
y = california_housing.target

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=0)

n = X_train.shape[0]
f = X_train.shape[1] + 1

# add column of 1 to X to handle bias
ones_column = np.ones((n, 1))
X_train = np.hstack((X_train, ones_column))
X_test = np.hstack((X_test, np.ones((X_test.shape[0], 1))))


B = 10
theta = 0.5


# inputs
#spoq_param = {"p": 0.75, "alpha": 7e-7, "q": 2, "eta": 0.1, "beta": 3e-3}

#for alpha in tqdm(np.logspace(-7, 2, num=8)):
for alpha in tqdm([1e-7]):
    for beta in tqdm([5]):
        for eta in tqdm([0.01]):

            print("** ALPHA, BETA, ETA : ", alpha, beta, eta)

            spoq_param = {"p": 0.75, "alpha": alpha, "q": 2, "eta": eta, "beta": beta}

            # start
            phi = lambda theta: np.mean((np.dot(X_train, theta) - y_train) ** 2)
            spoq = lambda x: spoq_penalty(np.array([x]), **spoq_param)

            p, alpha, q, eta, beta = (spoq_param[k] for k in ("p", "alpha", "q", "eta", "beta"))


            def prox(A, phi, x):
                pr_fn = lambda z: 0.5 * (np.dot((z - x).T, np.dot(A, z - x))) + phi(z)
                res = minimize(pr_fn, np.zeros_like(x)).x
                return res


            def gamma(n):
                return 1 / (n + 1)


            def trust_region(i, B, x, theta):
                if i == 1:
                    return sum(x**q)
                elif 2 <= i and i <= B - 1:
                    return theta ** (i - 1) * sum(x**q)
                else:
                    return 0


            Xi_q_pho = lambda gho: (q - 1) / ((eta**q + gho**q) ** (2 / q)) * np.eye(f)

            l_p_alpha = lambda x: np.sum((x**2 + alpha**2) ** (p / 2) - alpha**p)

            A_q_pho = lambda x, pho: Xi_q_pho(pho) + (1 / (l_p_alpha(x) + (beta**p))) * np.diag(
                (x**2 + alpha**2) ** ((p / 2) - 1)
            )

            def step(x, B, theta, g_k):
                for i in range(1, B + 1):
                    pho_k_i = trust_region(i, B, x, theta)
                    A_k_i = A_q_pho(x, pho_k_i)

                    xx = x - gamma(g_k) * np.dot(np.linalg.inv(A_k_i), gradlplq(x=x, alpha=alpha, beta=beta, mu=eta, p=p, q=q))

                    z_k_i = prox(1 / gamma(g_k) * A_k_i, phi, xx)

                    if sum(abs(z_k_i) ** q) > pho_k_i**q:
                        break
                return z_k_i


            def fb_algo(B, theta, nb_iter=1000):
                x = np.ones(f)
                g_k = 0

                all_x=[]

                for _ in tqdm(range(nb_iter)):
                    x = step(x, B, theta, g_k)
                    all_x.append(x)

                    g_k += 1

                    y_predict = X_test.dot(x)
                    mse_error = mean_squared_error(y_test, y_predict)

                    print("MSE :", mse_error)

                    if len(all_x)>3:
                        delta = np.linalg.norm((all_x[-1]- all_x[-2]))
                        if delta <= 1e-1:
                            break

                print("Nbr iter: ",g_k)
                return x

            #### TRAINING
            W_FB = fb_algo(B, theta)
            #print(list(W_FB))

            ## TESTING
            y_predict = X_test.dot(W_FB)
            mse_error = mean_squared_error(y_test, y_predict)

            print(f"TR-VMFB : MSE: {mse_error}, Sparcity: {sparcity(W_FB)}")


# lasso = Lasso(alpha=0.1, fit_intercept=False)
# ridge = Ridge(alpha=0.1, fit_intercept=False)

# lasso.fit(X, y)
# ridge.fit(X, y)

# # Évaluer les modèles
# lasso_pred = lasso.predict(X)
# ridge_pred = ridge.predict(X)

# # Calculer l'erreur quadratique moyenne pour chaque modèle
# lasso_mse = mean_squared_error(y, lasso_pred)
# ridge_mse = mean_squared_error(y, ridge_pred)

# # Afficher les résultats
# print(f"Lasso MSE: {lasso_mse}, Sparcity: {sparcity(lasso.coef_)}")
# print(f"Ridge MSE: {ridge_mse}, Sparcity: {sparcity(ridge.coef_)}")
