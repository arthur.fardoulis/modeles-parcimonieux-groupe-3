import matplotlib.pyplot as plt
import numpy as np

from utils import *


def plot_surface_and_contour(
    func_dict, rx_max=1, ry_max=1, point_number=100, save=False
):
    """
    Plot the surface and contour of given positive functions.
    """

    cmap = "magma"  # colormap

    x_vals = np.concatenate(
        (np.linspace(-rx_max, 0, point_number), np.linspace(0, rx_max, point_number))
    )
    y_vals = np.concatenate(
        (np.linspace(-ry_max, 0, point_number), np.linspace(0, ry_max, point_number))
    )
    X, Y = np.meshgrid(x_vals, y_vals)

    # Compute Z values for each function in the dictionary
    Z_dict = {}
    for name, func in func_dict.items():
        Z = np.zeros_like(X)
        for i in range(len(x_vals)):
            for j in range(len(y_vals)):
                Z[i, j] = func(X[i, j], Y[i, j])
        Z_dict[name] = Z

    # Normalize Z values for each function
    Z_min = min(np.min(Z) for Z in Z_dict.values())
    Z_max = max(np.max(Z) for Z in Z_dict.values())
    Z_normalized_dict = {
        name: (Z - Z_min) / (Z_max - Z_min)
        if Z_min != Z_max
        else Z / Z_max
        if Z_max != 0
        else Z
        for name, Z in Z_dict.items()
    }

    # Plot the surface and contour for each function
    num_functions = len(func_dict)
    fig = plt.figure(figsize=(6 * num_functions, 6))

    for i, (name, Z_normalized) in enumerate(Z_normalized_dict.items()):
        ax1 = fig.add_subplot(2, num_functions, i + 1, projection="3d")
        _ = ax1.plot_surface(X, Y, Z_normalized, cmap=cmap, linewidth=0)
        ax1.set_xlabel("X")
        ax1.set_ylabel("Y")
        ax1.set_zlabel("Function Value")
        ax1.set_title(f"{name} Surface Plot")
        ax1.view_init(elev=30, azim=45)

        ax2 = fig.add_subplot(2, num_functions, num_functions + i + 1)
        contour = ax2.contourf(X, Y, Z_normalized, cmap=cmap)
        ax2.set_xlabel("X")
        ax2.set_ylabel("Y")
        ax2.set_title(f"{name} Contour Plot")

    # Add color bar
    cbar = plt.colorbar(contour, ax=ax2)
    cbar.set_label("Normalized Function Value")

    plt.tight_layout()
    if save:
        plt.savefig("work\graphs\penalties_graph.png")
    plt.show()


if __name__ == "__main__":
    # Define SPOQ parameters
    spoq_param = {"p": 0.25, "alpha": 7e-7, "q": 2, "eta": 0.1, "beta": 3e-3}

    # Functions to plot
    spoq_2d = lambda x, y: spoq_penalty(np.array([x, y]), **spoq_param)
    l0_2d = lambda x, y: l0(np.array([x, y]))
    l1_2_2d = lambda x, y: l1_2(np.array([x, y]))
    l3_4_2d = lambda x, y: lp_q(np.array([x, y]), p=3, q=4)

    # Functions dict
    func_dict = {"L0": l0_2d, "SPOQ": spoq_2d, "L1_2": l1_2_2d, "L3_4": l3_4_2d}

    # Plot the functions
    plot_surface_and_contour(func_dict, save=True)
