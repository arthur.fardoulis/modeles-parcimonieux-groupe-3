## Here we implement MM algorithm
from scipy.optimize import minimize


def step(proxy_f, a):
    proxy_f_a = lambda x: proxy_f(a, x)
    na = minimize(proxy_f_a, a).x
    return na


def mm_algo(proxy_f, a, epsilon=0.01, nb_inter=100):
    na = step(proxy_f, a)
    it = 0
    while sum(abs(na - a)) > epsilon and it < nb_inter:
        a = na
        na = step(proxy_f, a)
        it += 1

    return na


if __name__ == "__main__":
    pass
