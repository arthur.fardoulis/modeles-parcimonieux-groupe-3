import numpy as np
from scipy.optimize import minimize
from sklearn.datasets import fetch_california_housing
from sklearn.linear_model import ElasticNet, Lasso, Ridge
from sklearn.metrics import mean_squared_error
from sklearn.model_selection import train_test_split
from tqdm import tqdm

from algo_MM import mm_algo
from utils import numerical_gradient, spoq_penalty


def sparcity(x, epslion=0.001):
    return np.sum(abs(x) < epslion)


# import dataset
california_housing = fetch_california_housing(as_frame=True)
X = california_housing.data
y = california_housing.target

n_lines = X.shape[0]
n_features = X.shape[1]

# add column of 1 to X to handle bias
ones_column = np.ones((n_lines, 1))
X = np.hstack((X, ones_column))

# split the data into training and testing sets
# X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=0)

# penalities to test
# models to fit
lasso = Lasso(alpha=0.1, fit_intercept=False)
ridge = Ridge(alpha=0.1, fit_intercept=False)
elastic_net = ElasticNet(alpha=0.1, l1_ratio=0.5, fit_intercept=False)


# Entraîner les modèles
lasso.fit(X, y)
ridge.fit(X, y)
elastic_net.fit(X, y)

# Évaluer les modèles
lasso_pred = lasso.predict(X)
ridge_pred = ridge.predict(X)
elastic_net_pred = elastic_net.predict(X)

# Calculer l'erreur quadratique moyenne pour chaque modèle
lasso_mse = mean_squared_error(y, lasso_pred)
ridge_mse = mean_squared_error(y, ridge_pred)
elastic_net_mse = mean_squared_error(y, elastic_net_pred)

# Afficher les résultats
print(f"Lasso MSE: {lasso_mse}, Sparcity: {sparcity(lasso.coef_)}")
print(f"Ridge MSE: {ridge_mse}, Sparcity: {sparcity(ridge.coef_)}")
print(f"ElasticNet MSE: {elastic_net_mse}, Sparcity: {sparcity(elastic_net.coef_)}")


print("SPOQ test")
# spoq penality
spoq_params = spoq_param = {"p": 0.75, "alpha": 1e-1, "q": 2, "eta": 0.1, "beta": 1e-2}
spoq = lambda x: spoq_penalty(x, **spoq_params)
mse = lambda x: np.mean((y - X.dot(x)) ** 2)

# change this to change influence of spoq in the term of penality
lamb = 10

penality = lambda x: spoq(x) + lamb * mse(x)

# solution using classical algorithms
# using SGD


# using minimize from Scipy
methods = ["nelder-mead", "BFGS", "L-BFGS-B"]

for method in methods:
    initial_guess = np.zeros(n_features + 1)
    result = minimize(fun=penality, x0=initial_guess, method=method)

    x = result.x
    y_predict = X.dot(x)
    mse_error = mean_squared_error(y, y_predict)

    print(f"MSE with {method} method: {mse_error}, Sparcity: {sparcity(x)}")


# using MM
spoq_params = spoq_param = {"p": 0.75, "alpha": 1e-1, "q": 2, "eta": 0.1, "beta": 1e-2}
p, alpha, q, eta, beta = (spoq_param[k] for k in ("p", "alpha", "q", "eta", "beta"))
L = lambda gho: (q - 1) / ((eta**q + gho**q) ** (2 / q)) + alpha ** (p - 2) * (
    beta ** (-p)
)

Xi_q_pho = (
    lambda gho: (q - 1) / ((eta**q + gho**q) ** (2 / q)) * np.eye(n_features + 1)
)
l_p_alpha = lambda x: np.sum((x**2 + alpha**2) ** (p / 2) - alpha**p)
A_pho_q = lambda x, pho: Xi_q_pho(pho) + (1 / (l_p_alpha(x) + (beta**p))) * np.diag(
    (x**2 + alpha**2) ** ((p / 2) - 1)
)


def proxy_f(a, x):
    result = (
        mse(x)
        + lamb * spoq(a)
        + lamb * (x - a).dot(numerical_gradient(spoq, np.array([a]))[0])
        + lamb / 2 * (x - a).dot(A_pho_q(a, 0)).dot(x - a)
    )
    return result


n_guesses = 10
initial_guesses = [np.random.rand(n_features + 1) for _ in range(n_guesses)]

min_mse = float("inf")
best_params = None

# Loop through each initial guess
for initial_guess in tqdm(initial_guesses, desc="Optimizing", total=n_guesses):
    x = mm_algo(proxy_f, initial_guess, epsilon=0.01, nb_inter=100)
    y_predict = X.dot(x)
    mse_error = mean_squared_error(y, y_predict)

    if mse_error < min_mse:
        min_mse = mse_error
        best_params = x

print(f"MSE with MM method: {mse_error}, Sparcity: {sparcity(best_params)}")


# Using FB
