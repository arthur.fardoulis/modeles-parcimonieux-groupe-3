import matplotlib.pyplot as plt
import numpy as np

from algo_MM import step
from utils import *


def plot_algorithm_1d(f, proxy_f, a, x_min=-3, x_max=5, point_number=300):
    x_vals = np.linspace(x_min, x_max, point_number)

    # Calculate the function values to determine y-axis limits
    f_vals = np.array([f(x) for x in x_vals])
    y_min, y_max = np.min(f_vals), np.max(f_vals)
    y_margin = (y_max - y_min) * 0.5

    # Set up the figure and axis
    fig, ax = plt.subplots(figsize=(12, 6))
    (line_f,) = ax.plot([], [], label="f(x)", color="blue")
    (line_proxy,) = ax.plot([], [], label="proxy_f", linestyle="--", color="red")
    point_f = ax.scatter([], [], color="blue", label="f(a)", zorder=5)
    point_proxy = ax.scatter([], [], color="red", label="min_proxy_f", zorder=5)
    green_line = None
    point_na = None

    # Function to update plot manually using keyboard
    def update_plot_manual(event):
        nonlocal a, green_line, point_na
        if event.key == " ":
            na = step(proxy_f, a).item()

            f_vals = np.array([f(x) for x in x_vals])
            proxy_vals = np.array([proxy_f(a, x) for x in x_vals])

            line_f.set_data(x_vals, f_vals)
            line_proxy.set_data(x_vals, proxy_vals)
            point_f.set_offsets(np.array([[a, f(a)]]))
            point_proxy.set_offsets(np.array([[na, proxy_f(a, na)]]))

            if green_line:
                green_line.remove()

            if point_na:
                point_na.remove()

            green_line = ax.axvline(
                x=na, color="purple", linestyle="--", label=f"vertical line at a"
            )
            point_na = ax.scatter(
                [na], [f(na)], color="green", label=f"({na}, f({na}))", zorder=5
            )

            a = na

            plt.draw()

    # Set plot properties
    ax.set_xlim(x_min, x_max)
    ax.set_ylim(y_min - y_margin, y_max + y_margin)
    ax.set_xlabel("x")
    ax.set_ylabel("Function Values")
    ax.set_title("Plot of f and proxy_f")
    ax.legend(loc="upper right", bbox_to_anchor=(1.05, 1))
    ax.grid(True)

    # Connect key event to function for manual iteration
    fig.canvas.mpl_connect("key_press_event", update_plot_manual)

    # Call update_plot_manual once to initiate the plot
    update_plot_manual(type("event", (), {"key": " "}))

    plt.show()
