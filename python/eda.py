import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
from sklearn.datasets import fetch_california_housing

california_housing = fetch_california_housing(as_frame=True)
X = california_housing.data
y = california_housing.target

print("X shape:", X.shape)
print("y shape:", y.shape)

print(california_housing.DESCR)

df = pd.DataFrame(california_housing.data, columns=california_housing.feature_names)
df["target"] = california_housing.target

print(df.info())

california_housing.frame.head()

plt.hist(y, bins=50)
plt.xlabel("Median house value")
plt.ylabel("Frequency")
plt.show()


corr_matrix = df.corr()

plt.figure(figsize=(10, 8))
sns.heatmap(corr_matrix, annot=True, cmap="coolwarm", linewidths=0.5)
plt.title("Correlation matrix")
plt.show()

features = ["MedInc", "HouseAge", "AveRooms", "Population"]

fig, axs = plt.subplots(nrows=2, ncols=2, figsize=(10, 8))

for i, feature in enumerate(features):
    row = i // 2
    col = i % 2
    sns.scatterplot(x=feature, y="target", data=df, ax=axs[row, col])
    axs[row, col].set_xlabel(feature)
    axs[row, col].set_ylabel("Median house value")

plt.tight_layout()
plt.show()

california_housing.frame.hist(figsize=(12, 10), bins=30, edgecolor="black")
plt.subplots_adjust(hspace=0.7, wspace=0.4)
