import numpy as np

l0 = lambda x: np.sum(x != 0)

lp_q = lambda x, p, q: np.sum(abs(x) ** p) ** (1 / p) / (np.sum(abs(x) ** q) ** (1 / q))

l1_2 = lambda x: lp_q(x, p=1, q=2)

l2_1 = lambda x: lp_q(x, p=2, q=1)


def spoq_penalty(x: np.array, p=0.25, q=2, alpha=7e-7, beta=3e-3, eta=1e-1):
    l_p_alpha = np.sum((x**2 + alpha**2) ** (p / 2) - alpha**p)
    l_q_eta = (eta**q + np.sum(np.abs(x) ** q)) ** (1 / q)
    psi_x = np.log((l_p_alpha + beta**p) ** (1 / p) / l_q_eta)
    return psi_x


def numerical_gradient(f, x, epsilon=1e-6):
    gradient = np.zeros_like(x)
    for i in range(len(x)):
        x_plus_eps = x.copy()
        x_plus_eps[i] += epsilon
        gradient[i] = (f(x_plus_eps) - f(x)) / epsilon
    return gradient

def gradlplq(x, alpha, beta, mu, p, q):
    """
    This function computes the gradient of smooth lp over lq function
    """
    # Define lp and lq
    lp = Lpsmooth(x, alpha, p)
    lq = Lqsmooth(x, mu, q)
    # Calculate grad1 and grad2
    grad1 = x * ((x**2 + alpha**2)**(p/2 - 1)) / (lp**p + beta**p)
    grad2 = np.sign(x) * np.abs(x)**(q - 1) / (lq**q)
    # Calculate and return gradphi
    gradphi = grad1 - grad2
    return gradphi


def Lpsmooth(x, alpha, p):
    """
    This function computes the smooth Lp norm of the vector x
    """
    # Calculate the smooth Lp norm
    lpx = (np.sum(((x**2) + (alpha**2))**(p/2) - (alpha**p)))**(1/p)
    return lpx

def Lqsmooth(x, mu, q):
    """
    This function computes the smooth Lq norm of the vector x
    """
    # Calculate the smooth Lq norm
    lqx = (mu**q + np.sum(np.abs(x)**q))**(1/q)
    return lqx