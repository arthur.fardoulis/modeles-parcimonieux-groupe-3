from plot_algorithm import plot_algorithm_1d
from utils import *

# case of cos function
freq = 2
f = lambda a: np.cos(freq * a)
proxy_f = (
    lambda a, x: np.cos(freq * a)
    - freq * np.sin(freq * a) * (x - a)
    + 0.5 * freq**2 * (x - a) ** 2
)

initial_a = 0.1  # float
plot_algorithm_1d(f, proxy_f, initial_a)


# case of SPOQ penality + MSE
spoq_param = {"p": 0.75, "alpha": 1e-1, "q": 2, "eta": 0.1, "beta": 1e-2}
lamb = 50

spoq = lambda x: lamb * spoq_penalty(np.array([x]), **spoq_param)

mse = lambda x: (2 - 1 * x) ** 2 + (5 - 4 * x) ** 2

f = lambda x: spoq(x) + mse(x)

p, alpha, q, eta, beta = (spoq_param[k] for k in ("p", "alpha", "q", "eta", "beta"))
L = lambda gho: (q - 1) / ((eta**q + gho**q) ** (2 / q)) + alpha ** (p - 2) * (
    beta ** (-p)
)

Xi_q_pho = lambda gho: (q - 1) / ((eta**q + gho**q) ** (2 / q))
l_p_alpha = lambda x: np.sum((x**2 + alpha**2) ** (p / 2) - alpha**p)
A_pho_q = lambda x, pho: Xi_q_pho(pho) + 1 / (l_p_alpha(x) + (beta**p)) * (
    x**2 + alpha**2
) ** (p / 2 - 1)


def proxy_f(a, x):
    result = (
        mse(x)
        + spoq(a)
        + (x - a) * (numerical_gradient(spoq, np.array([a])))
        + lamb * (A_pho_q(a, 0) / 2) * np.sum((x - a) ** 2)
    )
    return result.item()


initial_a = -1.0
plot_algorithm_1d(f, proxy_f, initial_a)
