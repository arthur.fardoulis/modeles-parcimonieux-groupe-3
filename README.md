# Modèles parcimonieux - Groupe 3 

Vous trouverez ici tout le code développé durant notre projet "Développement d'une nouvelle méthode de régression parcimonieuse basée sur une pénalité de rapport de normes". 
Divers méthodes d'optimisation ont été testées sur une fonction de coût avec pénalité SPOQ (voir Cherni al. 2020, https://arxiv.org/abs/2001.08496), afin de résoudre divers problèmes de régression et notamment celui du California Housing (https://inria.github.io/scikit-learn-mooc/python_scripts/datasets_california_housing.html).
Nous proposons notamment dans python/algo_FB.py une implémentation en python de l'algorithme TR-VMFB présenté dans Cherni al. 2020, originellement utilisé pour la reconstruction parcimonieuse de signaux. 